import React from 'react';
import ReactDOM from 'react-dom';
import Tetris from './Tetris';
import {StyledTetris} from "./styles/StyledTetris";

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Tetris />, div);
    ReactDOM.unmountComponentAtNode(div);
});
it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<StyledTetris />, div);
    ReactDOM.unmountComponentAtNode(div);
});