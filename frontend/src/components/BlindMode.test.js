import React from 'react';
import ReactDOM from 'react-dom';
import BlindMode from './BlindMode';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<BlindMode />, div);
    ReactDOM.unmountComponentAtNode(div);
});