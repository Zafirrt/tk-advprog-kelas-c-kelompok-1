import React from 'react';
import ReactDOM from 'react-dom';
import MoveBoardButton from './MoveBoardButton';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<MoveBoardButton />, div);
    ReactDOM.unmountComponentAtNode(div);
});