import React from 'react';
import ReactDOM from 'react-dom';
import Display from './Display';
import {StyledDisplay} from "./styles/StyledDisplay";

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Display />, div);
    ReactDOM.unmountComponentAtNode(div);
});

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<StyledDisplay />, div);
    ReactDOM.unmountComponentAtNode(div);
});