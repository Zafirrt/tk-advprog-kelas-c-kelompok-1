import React from 'react';
import { StyledDisplay } from './styles/StyledLeaderboard';

const Leaderboard = ({gameOver, text }) => (
    <StyledDisplay gameOver={gameOver}>{text}</StyledDisplay>
);

export default Leaderboard;
