import React from 'react';
import ReactDOM from 'react-dom';
import Stage from './Stage';
import {StyledStage} from "./styles/StyledStage";


it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<StyledStage />, div);
    ReactDOM.unmountComponentAtNode(div);
});